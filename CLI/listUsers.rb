puts
puts "🌼 Getting user list"
puts

#These should be assigned correct values
PROFILE="..."
USER_POOL_ID="..."

usersJson = `aws cognito-idp list-users --user-pool-id #{USER_POOL_ID} --limit 40 --region eu-west-1 --profile #{PROFILE}`
puts usersJson
