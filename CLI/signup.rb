require 'json'

puts
puts "🌼 Signing up the user"
puts

MFA_OPTION_SMS = "SMS"
MFA_OPTION_APP = "APP"

#These should be assigned correct values
APP_CLIENT_ID="..."
APP_CLIENT_SECRET="..."
PROFILE="..."
USER_POOL_ID="..."

# User details (input from user)
USERNAME="..." 
EMAIL="..."
PASSWORD="..."
PHONE_NUMBER="..."
PREFFERED_MFA=MFA_OPTION_SMS

# -------------------------
# SIGNUP
# -------------------------

puts `aws cognito-idp admin-create-user \
    --user-pool-id #{USER_POOL_ID} \
    --username #{USERNAME} \
    --user-attributes Name=email,Value=#{EMAIL} Name=email_verified,Value=true\
    --message-action SUPPRESS \
    --profile #{PROFILE} \
    --region eu-west-1`

puts `aws cognito-idp admin-set-user-password \
    --user-pool-id #{USER_POOL_ID} \
    --username #{USERNAME}\
    --password #{PASSWORD} \
    --permanent \
    --profile #{PROFILE} \
    --region eu-west-1`

# -------------------------
# SET MFA
# -------------------------


if (PREFFERED_MFA == MFA_OPTION_SMS) # SMS MFA
    #1. update phone number
    puts '🎈admin-update-user-attributes'
    puts `aws cognito-idp admin-update-user-attributes \
        --user-pool-id #{USER_POOL_ID} \
        --username #{USERNAME}\
        --user-attributes Name="phone_number",Value="#{PHONE_NUMBER}" Name="phone_number_verified",Value="true" \
        --profile #{PROFILE} \
        --region eu-west-1`

    #2. set mfa preference
    puts '🎈admin-set-user-mfa-preference'
    puts `aws cognito-idp admin-set-user-mfa-preference \
        --user-pool-id #{USER_POOL_ID} \
        --username #{USERNAME}\
        --sms-mfa-settings Enabled=true,PreferredMfa=true \
        --profile #{PROFILE} \
        --region eu-west-1`

    #3. initiate auth
    puts '🎈admin-initiate-auth'
    COMPUTED_HASH = `printf '#{USERNAME}#{APP_CLIENT_ID}' | openssl dgst -sha256 -hmac #{APP_CLIENT_SECRET} -binary | openssl enc -base64`.strip

    authResult = `aws cognito-idp admin-initiate-auth \
        --user-pool-id #{USER_POOL_ID} \
        --client-id #{APP_CLIENT_ID} \
        --auth-flow ADMIN_NO_SRP_AUTH \
        --auth-parameters USERNAME=#{USERNAME},PASSWORD=#{PASSWORD},SECRET_HASH=#{COMPUTED_HASH} \
        --profile #{PROFILE} \
        --region eu-west-1`
    
    puts '🎈'+authResult
    parsedResult = JSON.parse(authResult)
        
    session = parsedResult["Session"]
    # refresh_token = parsedResult["AuthenticationResult"]["RefreshToken"]
    # id_token = parsedResult["AuthenticationResult"]["IdToken"]

    # puts '🎈'+access_token
    # puts '🎈'+refresh_token
    # puts '🎈'+id_token

    #4. Respond to mfa challenge
    puts "Wite your code:"
    mfa_code = gets.chomp

    puts `aws cognito-idp admin-respond-to-auth-challenge \
        --user-pool-id #{USER_POOL_ID} \
        --client-id #{APP_CLIENT_ID} \
        --challenge-name SMS_MFA \
        --session #{session} \
        --challenge-responses SMS_MFA_CODE=#{mfa_code},USERNAME=#{USERNAME},SECRET_HASH=#{COMPUTED_HASH} \
        --profile #{PROFILE} \
        --region eu-west-1`
    # weird that in cognito console mfa doesnt look like its enabled, but I still get the challenge at initiate auth
    # https://stackoverflow.com/questions/64685783/is-sms-mfa-status-in-cognito-user-pools-set-by-calling-setpreferredmfa-or-is-tha
else #AUTHENTICATOR MFA
    #1. initiate auth
    puts '🎈admin-initiate-auth'
    COMPUTED_HASH = `printf '#{USERNAME}#{APP_CLIENT_ID}' | openssl dgst -sha256 -hmac #{APP_CLIENT_SECRET} -binary | openssl enc -base64`.strip

    authResult = `aws cognito-idp admin-initiate-auth \
        --user-pool-id #{USER_POOL_ID} \
        --client-id #{APP_CLIENT_ID} \
        --auth-flow ADMIN_NO_SRP_AUTH \
        --auth-parameters USERNAME=#{USERNAME},PASSWORD=#{PASSWORD},SECRET_HASH=#{COMPUTED_HASH} \
        --profile #{PROFILE} \
        --region eu-west-1`

    puts '🎈'+authResult
    parsedResult = JSON.parse(authResult)
    access_token = parsedResult["AuthenticationResult"]["AccessToken"]

    #2. associate-software-token
    puts '🎈associate-software-token'
    puts`aws cognito-idp associate-software-token \
        --access-token #{access_token} \
        --profile #{PROFILE} \
        --region eu-west-1`
    #user should introduce this code in the authentication app then write the 6 digit code (for image version we need to find another way, but I guess it's similar)
    puts "Wite your 6 digits code:"
    mfa_code = gets.chomp

    #3. verify-software-token
    puts `aws cognito-idp verify-software-token \
        --access-token #{access_token} \
        --user-code #{mfa_code} \
        --profile #{PROFILE} \
        --region eu-west-1`

    #4. set mfa preference
    puts '🎈admin-set-user-mfa-preference'
    puts `aws cognito-idp admin-set-user-mfa-preference \
        --user-pool-id #{USER_POOL_ID} \
        --username #{USERNAME}\
        --software-token-mfa-settings Enabled=true,PreferredMfa=true \
        --profile #{PROFILE} \
        --region eu-west-1`


    #5 initiate auth
    puts '🎈admin-initiate-auth'
    authResult = `aws cognito-idp admin-initiate-auth \
        --user-pool-id #{USER_POOL_ID} \
        --client-id #{APP_CLIENT_ID} \
        --auth-flow ADMIN_NO_SRP_AUTH \
        --auth-parameters USERNAME=#{USERNAME},PASSWORD=#{PASSWORD},SECRET_HASH=#{COMPUTED_HASH} \
        --profile #{PROFILE} \
        --region eu-west-1`

    puts '🎈'+authResult
    parsedResult = JSON.parse(authResult)
    # parsedResult["ChallengeName"] will be SOFTWARE_TOKEN_MFA
    session = parsedResult["Session"]

    #6. Respond to mfa challenge
    puts "Wite your 6 digit code:"
    mfa_code = gets.chomp

    puts `aws cognito-idp admin-respond-to-auth-challenge \
        --user-pool-id #{USER_POOL_ID} \
        --client-id #{APP_CLIENT_ID} \
        --challenge-name SOFTWARE_TOKEN_MFA \
        --session #{session} \
        --challenge-responses SOFTWARE_TOKEN_MFA_CODE=#{mfa_code},USERNAME=#{USERNAME},SECRET_HASH=#{COMPUTED_HASH} \
        --profile #{PROFILE} \
        --region eu-west-1`
end

#Note
# - to make sure that user has mfa activated, at admin-initiate-auth, the response should contain a "ChallengeName" node
#   and it should be of value SOFTWARE_TOKEN_MFA or SMS_MFA